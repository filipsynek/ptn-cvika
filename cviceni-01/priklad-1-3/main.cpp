// Příklad 1.3
#include <iostream>

const int VELIKOST_MATICE = 100;

typedef int TypRadek[VELIKOST_MATICE];
typedef TypRadek TypMatice[VELIKOST_MATICE];

void doplnMaticiNulami(TypMatice &matice, unsigned int radek, unsigned int sloupec, unsigned int pocetRadku, unsigned int pocetSloupcu) {
    while (radek < pocetRadku) {
        matice[radek][sloupec] = 0;
        sloupec++;
        if (sloupec >= pocetSloupcu) {
            sloupec = 0;
            radek++;
        }
    }
}

void vypisMatici(const TypMatice &matice, unsigned int pocetRadku, unsigned int pocetSloupcu) {
    for (int r = 0; r < pocetRadku; ++r) {
        for (int s = 0; s < pocetSloupcu; ++s) {
            std::cout << matice[r][s] << " ";
        }
        std::cout << std::endl;
    }
}

int main() {
    // Deklarace proměnných
    unsigned int pocetRadku;
    unsigned int pocetSloupcu;
    unsigned int radek = 0;
    unsigned int sloupec = 0;
    int hodnota = 0;
    TypMatice matice;

    // Zadání velikosti matice
    std::cout << "Zadejte počet řádků a sloupců" << std::endl;
    std::cin >> pocetRadku >> pocetSloupcu;

    // Načítání hodnot
    // Cyklus se opakuje dokud uživatel zadává hodnoty nebo není překročen
    // maximální počet řádků
    std::cout << "Začněte zadávat hodnoty matice:" << std::endl;
    while ((std::cin >> hodnota) and (radek < pocetRadku)) {
        matice[radek][sloupec] = hodnota;
        sloupec++;
        if (sloupec >= pocetSloupcu) {
            sloupec = 0;
            radek++;
        }
    }

    // Po načítání hodnot se do nedoplněných buněk doplní nuly
    doplnMaticiNulami(matice, radek, sloupec, pocetRadku, pocetSloupcu);

    //vypsání výsledné matice
    vypisMatici(matice, pocetRadku, pocetSloupcu);
}
