// Příklad 1.4
// Na každém řádku standardního vstupu je zadán název zboží. Vypište tento seznam názvů na standardní výstup v obráceném pořadí.
#include <iostream>

const int VELIKOST_POLE = 128;
typedef std::string TypPole[VELIKOST_POLE];

void vypisNazvyPozpatku(TypPole pole, unsigned int velikostPole) {
    for (int i = velikostPole - 1; i >= 0; --i) {
        std::cout << i << "\t"
                  << "\"" << pole[i] << "\""
                  << " " << std::endl;
    }
    std::cout << std::endl;
}

int main() {
    // Deklarace proměnných
    TypPole pole;
    std::string nazevZbozi;
    unsigned int pocitadlo = 0;

    std::cout << "Zadejte názvy" << std::endl;
    while (std::getline(std::cin, nazevZbozi) and (nazevZbozi != "") and (pocitadlo < VELIKOST_POLE)) {
        pole[pocitadlo] = nazevZbozi;
        ++pocitadlo;
    }

    vypisNazvyPozpatku(pole, pocitadlo);
    return 0;
}
