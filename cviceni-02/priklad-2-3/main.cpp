#include <iostream>

const unsigned int VELIKOST_MATICE = 64;
typedef float TypVektor[VELIKOST_MATICE];

float soucetPrvkuVektoru(const TypVektor &pole, unsigned int pocetPrvku){
    float soucet = 0;
    
    for (int i = 0; i<(pocetPrvku); ++i ){
        soucet += pole[i];
    }

    return soucet;
}

void nactiHodnotyVektoru(TypVektor& pole, unsigned int& pocitadlo){
    float vstup;
    pocitadlo = 0;
    std::cout<<"Zadej hodnoty matice: "<<std::endl;
    while((std::cin>>vstup) and (pocitadlo < VELIKOST_MATICE-1)){
        pole[pocitadlo] = vstup;
        pocitadlo++;
    }
}

void vypisVektor(const TypVektor& pole, unsigned int pocetPrvku){
    for (int i = 0; i<(pocetPrvku); ++i){
        std::cout << pole[i] << "; ";
    }
    std::cout<<std::endl;
}

int main(){
    TypVektor v;
    unsigned int pocitadlo = 0;
    nactiHodnotyVektoru(v,pocitadlo);
    vypisVektor(v, pocitadlo);
    std::cout<<"Součet prvků vektoru je: "<< soucetPrvkuVektoru(v, pocitadlo) << std::endl;

    return 0;
}