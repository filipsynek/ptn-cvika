#include <iostream>

const unsigned int VELIKOST_MATICE = 64;
typedef int TypPole[VELIKOST_MATICE];

void vypisPole(const TypPole &pole, int pocetPrvku){
    for (int i = 0; i<pocetPrvku; ++i){
        std::cout<<pole[i]<<"; ";
    } 
    std::cout<<std::endl;
}

void nactiPole(TypPole &pole, int &pocetPrvku){
    // Inicializace proměnných
    int vstup = 0;
    pocetPrvku = 0; 
    
    std::cout<<"Zadej prvky pole: "<<std::endl;
    while((std::cin>>vstup) and (pocetPrvku<VELIKOST_MATICE)){
        pole[pocetPrvku] = vstup;
        ++pocetPrvku;
    }
}


int main(){
    TypPole pole;
    int pocetPrvku = 0;
    nactiPole(pole,pocetPrvku);
    vypisPole(pole,pocetPrvku);

    return 0;
} 