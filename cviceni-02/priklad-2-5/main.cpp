#include <iostream>

typedef struct {
    char nazev[20]; // Název zboží
    float cena; // Cena za kus zboží
    unsigned int pocet; // Počet kusů zboží na skladě
} TypZbozi;
const unsigned int MAXIMALNI_POCET_ZAZNAMU = 64;
const unsigned int MINIMALNI_POCET_ZBOZI = 5;
typedef TypZbozi TypSklad[MAXIMALNI_POCET_ZAZNAMU];


void vypisNadprumerneMnozstvi(const TypSklad &sklad, int pocetPolozek){
    int soucetPolozek = 0;
    for (int i = 0; i<pocetPolozek; ++i){
        soucetPolozek += sklad[i].pocet;
    }

    float prumernyPocetPolozek = (float) soucetPolozek/pocetPolozek;

    std::cout<<"\nZboží s nadprůměrným počtem kusů na skladě: "<<std::endl;
    for (int i = 0; i<pocetPolozek; ++i){
        if (sklad[i].pocet > prumernyPocetPolozek){
            std::cout<<sklad[i].nazev<<std::endl;
        }
    }
}

void vypisZboziPodlePoctu(const TypSklad &sklad, int pocetPolozek){
    std::cout<<"\nZboží, které má na skladě méně než "<<MINIMALNI_POCET_ZBOZI<<" kusů na skladě"<<std::endl;
    for (int i = 0; i<pocetPolozek; ++i){
        if (sklad[i].pocet < MINIMALNI_POCET_ZBOZI){
            std::cout<<sklad[i].nazev<<"("<<sklad[i].pocet<<" kusů)"<<std::endl;
        }
    }
}

int main(){
    TypSklad sklad;
    unsigned int pocitadlo = 0;
    std::cout<<"Název \t Cena za kus \t Počet"<<std::endl;
    while((std::cin >> sklad[pocitadlo].nazev) and (pocitadlo < MAXIMALNI_POCET_ZAZNAMU)){
        std::cin >> sklad[pocitadlo].cena >> sklad[pocitadlo].pocet;
        pocitadlo++;
    }
    vypisNadprumerneMnozstvi(sklad, pocitadlo);
    vypisZboziPodlePoctu(sklad,pocitadlo);
    return 0;
}