#include <iostream>

const unsigned int MAXIMALNI_VELIKOST_POLE = 64;
typedef float TypPole[MAXIMALNI_VELIKOST_POLE];

void seradPole(TypPole &pole, unsigned int velikostPole){
    // Řazení přímým vkládáním neboli "Insert sort"
    // Proběhne sestupné řazení
    for (int i = 0; i < (velikostPole); ++i){
        int j = i + 1;
        int temp = pole[j];
        while ((j>0) and (temp > pole[j-1])){
            pole[j] = pole[j-1];
            j--;
        }
        pole[j] = temp;
    }
}

void vypisPole(const TypPole &pole, unsigned int velikostPole){
    for (int i = 0; i < (velikostPole); ++i){
        std::cout<<pole[i]<<"; "; 
    }
    std::cout<<std::endl;
}

int main(){
    TypPole pole;
    unsigned int velikostPole = 0;

    while((std::cin>>pole[velikostPole]) and (velikostPole < MAXIMALNI_VELIKOST_POLE)){
        ++velikostPole;
    }
    vypisPole(pole, velikostPole);
    seradPole(pole, velikostPole);
    vypisPole(pole, velikostPole);
    return 0;
}