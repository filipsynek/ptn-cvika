#include <iostream>

const unsigned int MAXIMALNI_VELIKOST_POLE = 64;
typedef float TypPole[MAXIMALNI_VELIKOST_POLE];

void seradPole(TypPole &pole, unsigned int velikostPole){
    // Řazení přímým výběrem neboli "Selection sort"
    for (int i = 0; i < velikostPole; ++i){
        int maxIndex = i;
        for (int j = i + 1; j < velikostPole; ++j){
            if (pole[j] > pole[maxIndex]) {
                maxIndex = j;
            }
        }
        int temp = pole[i];
        pole[i] = pole[maxIndex];
        pole[maxIndex] = temp;
    }
}

void vypisPole(const TypPole &pole, unsigned int velikostPole){
    for (int i = 0; i < (velikostPole); ++i){
        std::cout<<pole[i]<<"; "; 
    }
    std::cout<<std::endl;
}

int main(){
    TypPole pole;
    unsigned int velikostPole = 0;

    while((std::cin>>pole[velikostPole]) and (velikostPole < MAXIMALNI_VELIKOST_POLE)){
        ++velikostPole;
    }
    vypisPole(pole, velikostPole);
    seradPole(pole, velikostPole);
    vypisPole(pole, velikostPole);
    return 0;
}