#include <iostream>
#include <fstream>

int main() {
    const unsigned int MAXIMALNI_POCET_ZAMESTNANCU = 64;
    typedef struct {
        char jmeno[13];
        int pracoviste;
        double vykon;
    } TypZamestnanec;
    typedef TypZamestnanec TypFirma[MAXIMALNI_POCET_ZAMESTNANCU];

    // Deklarace proměnných
    std::fstream vystup("firma.dat", std::ios::out | std::ios::binary);
    TypFirma firma;
    int pocitadlo = 0;

    // std::cout << "Jméno \t Pracoviště \t Výkon" << std::endl
    while (std::cin >> firma[pocitadlo].jmeno >> firma[pocitadlo].pracoviste >> firma[pocitadlo].vykon) pocitadlo++;

    // Jako první zapíšeme počet zaměstnanců, které uložíme do souboru.
    // Tento postup nám poslouží při opětovném čtení, když budeme přesně vědět kolik
    // informací se v souboru nachází.
    // hodnota uložená v proměnné `pocitadlo` je přetypovaná na znakový typ
    vystup.write((char*)&pocitadlo, sizeof(pocitadlo));

    // V tomto kroku zapíšeme kompletní strukturu `firma`
    // Do souboru se zapíše počet bajtů, který zabírá jedna položka `TypZamestnanec`,
    // ze které se skládá pole `firma` a tato hodnota se vynásobí počtem zaměstnanců
    // uloženým v proměnné `pocitadlo`
    vystup.write((char*)firma, sizeof(TypZamestnanec) * pocitadlo);

    vystup.close();

    return 0;
}