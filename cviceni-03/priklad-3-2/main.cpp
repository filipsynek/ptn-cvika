#include <fstream>
#include <iostream>

int main() {
    std::ifstream retezce("retezce.dat", std::ios::binary);
    std::string retez;
    char znak;
    int max = 0;

    while (!retezce.eof()) {
        retez = "";
        znak = retezce.get();
        while ((znak != 0) and not retezce.eof()) {
            retez += znak;
            znak = retezce.get();
        }
        if (retez.length() > max) {
            max = retez.length();
        }
    }
    std::cout << "Nejdelší řetězec má "<< max << " bajtů." << std::endl; 
    return 0;
}