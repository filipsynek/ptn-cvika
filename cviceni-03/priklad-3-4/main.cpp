#include <iostream>
#include <fstream>

int main(){
    std::ifstream vstup("data.txt");
    std::fstream vystup("retezce.dat", std::ios::binary | std::ios::out);

    if (vstup.is_open()) {
        char pole[80];

        while (!vstup.eof()){
            vstup.getline(pole, sizeof(pole));
            vystup.write((char*)pole, sizeof(pole));
        }

    } else {
        std::cerr << "Soubor data.txt se nepovedlo otevřít" << std::endl;
    }

    vstup.close();
    vystup.close();

}