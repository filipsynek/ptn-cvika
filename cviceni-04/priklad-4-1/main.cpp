#include <fstream>
#include <iostream>

const int k_DelkaHesla = 3;

int main() {
    // Deklarace proměnných
    std::fstream souborHeslo("heslo.txt", std::ios::in);
    std::fstream souborSifra("sifra.cif", std::ios::out | std::ios::binary);
    char heslo[k_DelkaHesla];
    char znak;
    char sifrovanyZnak;
    unsigned int indexHesla = 0;

    if (souborHeslo.is_open()) {
        souborHeslo >> heslo;
        while (std::cin.get(znak)) {
            sifrovanyZnak = znak ^ heslo[indexHesla];
            souborSifra.put(sifrovanyZnak);

            if (indexHesla == (k_DelkaHesla - 1)) {
                indexHesla = 0;
            } else {
                indexHesla++;
            }
        }
        souborSifra.close();
    } else {
        std::cerr << "Soubor heslo.txt se nepodařilo otevřít" << std::endl;
    }

	return 0;
}