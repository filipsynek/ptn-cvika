#include <iostream>

const int k_pocetCisel = 32;

int main() {
    unsigned long int mnozina = 0;  // 0000 0000 0000 0000 0000 0000 0000 0000
    unsigned long int masky[k_pocetCisel], hodnotaMasky;
    hodnotaMasky = 1;

    // Nastavení jednotlivých masek
    for (int i = 0; i < k_pocetCisel; i++) {
        masky[i] = hodnotaMasky;
        hodnotaMasky <<= 1;  // obdoba zápisu m = m << 1
    }

    // Získání vstupu od uživatele
    unsigned int cislo = 0;
    std::cout << "Zadejte čísla z intervalu <0;" << (k_pocetCisel - 1) << ">: " << std::endl;
    while (std::cin >> cislo) {
        if ((cislo >= 0) and (cislo <= (k_pocetCisel - 1))) {
            mnozina |= masky[cislo];
        }
    }

    // Vypsání čísel, která se nevyskytovala na vstupu
    std::cout << "Na vstupu se nevyskytovala tato čísla: " << std::endl;
    for (int i = 0; i < (k_pocetCisel); i++) {
        if ((mnozina & masky[i]) == 0) {
            std::cout << i << std::endl;
        }
    }

    return 0;
}