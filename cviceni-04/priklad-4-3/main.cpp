#include <fstream>
#include <iostream>

const int k_pocetCisel = 64;  //Interval <0;63>

int main() {
    // Deklarace proměnných
    unsigned long long int mnozina = 0;  // Množina vstupů - je modifikována podle číslel, které uživatel zadá. Zpočátku je prázdná
    unsigned long long int masky[k_pocetCisel], hodnotaMasky = 1;

    // Načtení bitových masek
    for (int i = 0; i < k_pocetCisel; i++) {
        masky[i] = hodnotaMasky;
        hodnotaMasky <<= 1;

    }

    unsigned int cislo = 0;
    while (std::cin >> cislo) {
        if ((cislo >= 0) and (cislo <= (k_pocetCisel - 1))) {
            mnozina |= masky[cislo];
        }
    }

    // Výstup seřazených vstupních čísel
    for (int i = 0; i < k_pocetCisel; i++) {
        if (mnozina & masky[i]) {
            std::cout << i << std::endl;
        }
    }

    return 0;
}