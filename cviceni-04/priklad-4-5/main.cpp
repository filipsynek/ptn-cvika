#include <iostream>
#include <fstream>

char zabezpeciBit(char c){
	int pocitadloJednicek = 0;
	char temp = c;
	while(temp > 0) {
		pocitadloJednicek += temp & 1;
		temp >>= 1;
	}

	if ((pocitadloJednicek % 2) > 0) {
		c |=  0x80;	
	}

	return c;
}


int main(){
	char vstup;
	std::fstream vystup("parita.dat", std::ios::out | std::ios::binary);
	while (std::cin.get(vstup) ) {
		vystup.put(zabezpeciBit(vstup));
	
	}

	vystup.close();
	return 0;
}
