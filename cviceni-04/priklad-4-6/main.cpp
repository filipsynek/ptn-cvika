#include <iostream>
#include <fstream>

int main(){
	std::fstream vstup("ucto.xxx", std::ios::in | std::ios::binary);
	char podelnaParita = 0;
	char znak;
	if (vstup.is_open()){
		
		while (vstup.get(znak)) {
			podelnaParita ^= znak;

		}
		
		std::cout << (int) podelnaParita << std::endl;
		vstup.close();
	} else {
		std::cerr << "Soubor \"ucto.xxx\" se nepodařilo otevřít" << std::endl; 
	}
	return 0;
}
